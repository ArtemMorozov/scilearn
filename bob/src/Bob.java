/**
 * Created by @author Artem Morozov on 3/1/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Bob {
    public Bob() {
    }

    /** Method hey with Bob's replies
     * @param input input specified by user which is used to get reply from Bob
     * @return 'Sure.' if input contains a question mark at the end.
     * return 'Whoa, chill out!' if entire input is upper case string.
     * return 'Fine. Be that way!' if input is empty.
     * return 'Whatever.' to anything else. */
    protected String hey(String input) {
        if (input.trim().endsWith("?")) {
            return "Sure.";
        } else if (isUpperCase(input) && !input.isEmpty() &&
                input.trim().length() > 0) {
            return "Whoa, chill out!";
        } else if (input.trim().length() == 0 && !input.isEmpty()) {
            return "Fine. Be that way!";
        } else {
            return "Whatever.";
        }
    }

    /** Boolean to be used to get a reply from Bob in case input string is all upper case string
     * @param s input from user
     * @return true if string s is upper case */
    private boolean isUpperCase(String s) {
        char[] arr = s.toCharArray();
        for (char c : arr) {
            if (Character.isLetter(c)) {
                if (!Character.isUpperCase(c)) {
                    return false;
                }
            }
        }
        return true;
    }
} // end of Bob