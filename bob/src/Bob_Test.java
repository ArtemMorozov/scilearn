import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by @author Artem Morozov on 3/3/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Bob_Test {
    private Bob bob = new Bob();
    private String expected;

    @Test
    public void test_Yelling() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("HASHTAGTESTONE"));
    }

    @Test
    public void test_Yelling_singleLetter() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("O"));
    }

    @Test
    public void test_Yelling_withSpace() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("HI THERE"));
    }

    @Test
    public void test_Yelling_withSpaces() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("        SPAAAAACEEEEES"));
    }

    @Test
    public void test_Yelling_withDigit() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("H3LLO"));
    }

    @Test
    public void test_Yelling_withCharacters() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("O-O-OH COME-E-E O-O-O-ON!%^*@#$(*^"));
    }

    @Test
    public void test_Yelling_withTab() {
        expected = "Whoa, chill out!";
        assertEquals(expected, bob.hey("TAAAAAAAAB\t"));
    }

    @Test
    public void test_SayNothing_singleSpace() {
        expected = "Fine. Be that way!";
        assertEquals(expected, bob.hey(" "));
    }

    @Test
    public void test_SayNothing_Spaces() {
        expected = "Fine. Be that way!";
        assertEquals(expected, bob.hey("    "));
    }

    @Test
    public void test_SayNothing_singleTab() {
        expected = "Fine. Be that way!";
        assertEquals(expected, bob.hey("\t"));
    }

    @Test
    public void test_SayNothing_Tabs() {
        expected = "Fine. Be that way!";
        assertEquals(expected, bob.hey("\t\t"));
    }

    @Test
    public void test_Question() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("Do you want coffee?"));
    }

    @Test
    public void test_Question_withDigitsOnly() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("2?"));
    }

    @Test
    public void test_Question_withCharacters() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("%^*@#$(*^?"));
    }

    @Test
    public void test_Question_withSpaces() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("Today?   "));
    }

    @Test
    public void test_Question_withTabs() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("Tomorrow?\t\t"));
    }

    @Test
    public void test_Question_double() {
        expected = "Sure.";
        assertEquals(expected, bob.hey("how r u??"));
    }

    @Test
    public void test_Whatever() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("whatever"));
    }

    @Test
    public void test_Whatever_trickyQuestion() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("Is it ? a question"));
    }

    @Test
    public void test_Whatever_emptyString() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey(""));
    }

    @Test
    public void test_Whatever_spaces() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("hi spaces    "));
    }

    @Test
    public void test_Whatever_leadingSpaces() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("      hi leading spaces"));
    }

    @Test
    public void test_Whatever_tabs() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("hi tabs\t\t\t"));
    }

    @Test
    public void test_Whatever_leadingTabs() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("\t\thi leading tabs"));
    }

    @Test
    public void test_Whatever_paragraph() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("hi paragraph\n"));
    }

    @Test
    public void test_Whatever_leadingParagraph() {
        expected = "Whatever.";
        assertEquals(expected, bob.hey("\nhi paragraph"));
    }
} // end of Bob_Test