/**
 * Created by @author Artem Morozov on 3/1/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class PhoneNumber {
    private String phoneNumber;

    /** Constructor which will return an Exception when number is invalid
     * @param num phone number entered by user when object of class is created
     */
    public PhoneNumber(String num) throws Exception {
        try {
            if (num.length() < 10 || num.length() > 11) {
                throw new Exception("Invalid length of phone number. Try entering 10 digits number or 11 with '1' as a first digit.");
            } else if (num.length() == 10) {
                this.phoneNumber = num;
            } else if (num.length() == 11) {
                if (Character.digit(num.charAt(0), 9) == 1) {
                    // assigning 10 digit number to new variable after trimming '1'
                    this.phoneNumber = num.substring(1);
                } else {
                    throw new Exception("Invalid leading digit. Try entering 11 digits number with '1' as a first digit.");
                }
            }
        } catch (Exception e) {
            e.getStackTrace();
            throw e;
        }
    }

    /** @return a string representation of all 10 numerical digits. EG "4158675309"
     * i.e. return original phoneNumber */
    protected String number() {
        return phoneNumber;
    }

    /** @return a string of the first three digits EG "415"
     * */
    protected String areaCode() {
        return phoneNumber.substring(0, 3);
    }

    /** @return a string of the next three digits EG "867"
     */
    protected String exchangeCode() {
        return phoneNumber.substring(3, 6);
    }

    /** @return a string of the last four digits EG "5309"
     */
    protected String lineNumber() {
        return phoneNumber.substring(6, 10);
    }

    /** Creating a StringBuilder variable with limit of 10 (as giving number per task), will expand that by itself during adding
     * Creating a char array to go over a user input in loop and adding characters in right spots
     * When done will overwrite a @String phoneNumber used in other methods
     * @return the number formatted as "(XXX) XXX-XXXX" EG "(415) 867-5309"
     */
    protected String pretty() {
        StringBuilder sb = new StringBuilder(10);
        char[] array = phoneNumber.toCharArray();
        sb.append("(");
        sb.append(areaCode());
        sb.append(") ");
        sb.append(exchangeCode());
        sb.append("-");
        sb.append(lineNumber());
        phoneNumber = sb.toString();
        return phoneNumber;
    }
} // end of PhoneNumber