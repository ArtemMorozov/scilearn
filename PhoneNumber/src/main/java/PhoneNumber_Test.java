import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by @author Artem Morozov on 3/3/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class PhoneNumber_Test {
    private PhoneNumber phoneNumber;
    private String expected;
    private String actual;

    public PhoneNumber_Test() throws Exception {
    }

    @Test
    public void test_Below10Digits_1() {
        expected = "Invalid length of phone number. Try entering 10 digits number or 11 with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("444555678");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_Above11Digits_1() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("44455567890");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_number() {
        expected = "4445556789";
        try {
            phoneNumber = new PhoneNumber("4445556789");
        } catch (Exception e) {
            actual = phoneNumber.number();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_areaCode() {
        expected = "444";
        try {
            phoneNumber = new PhoneNumber("4445556789");
        } catch (Exception e) {
            actual = phoneNumber.areaCode();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_exchangeCode() {
        expected = "555";
        try {
            phoneNumber = new PhoneNumber("4445556789");
        } catch (Exception e) {
            actual = phoneNumber.exchangeCode();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_lineNumber() {
        expected = "6789";
        try {
            phoneNumber = new PhoneNumber("4445556789");
        } catch (Exception e) {
            actual = phoneNumber.lineNumber();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_pretty() {
        expected = "(444) 555-6789";
        try {
            phoneNumber = new PhoneNumber("4445556789");
        } catch (Exception e) {
            actual = phoneNumber.pretty();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_number() {
        expected = "4445556789";
        try {
            phoneNumber = new PhoneNumber("14445556789");
        } catch (Exception e) {
            actual = phoneNumber.number();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_areaCode() {
        expected = "444";
        try {
            phoneNumber = new PhoneNumber("14445556789");
        } catch (Exception e) {
            actual = phoneNumber.areaCode();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_exchangeCode() {
        expected = "555";
        try {
            phoneNumber = new PhoneNumber("14445556789");
        } catch (Exception e) {
            actual = phoneNumber.exchangeCode();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_lineNumber() {
        expected = "6789";
        try {
            phoneNumber = new PhoneNumber("14445556789");
        } catch (Exception e) {
            actual = phoneNumber.lineNumber();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_pretty() {
        expected = "(444) 555-6789";
        try {
            phoneNumber = new PhoneNumber("14445556789");
        } catch (Exception e) {
            actual = phoneNumber.pretty();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_Invalid_Leading_Digit0() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("04445556789");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_Invalid_Leading_Digit2() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("24445556789");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_Invalid_Leading_Digit9() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("94445556789");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_Negative() {
        expected = "Invalid length of phone number. Try entering 10 digits number or 11 with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("-14445556789");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_Negative() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("-4445556789");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_11Digits_with_Letter() {
        expected = "Invalid length of phone number. Try entering 10 digits number or 11 with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("14445556789a");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void test_10Digits_with_Letter() {
        expected = "Invalid leading digit. Try entering 11 digits number with '1' as a first digit.";
        try {
            phoneNumber = new PhoneNumber("4445556789a");
        } catch (Exception e) {
            actual = e.getLocalizedMessage();
            assertEquals(expected, actual);
        }
    }
} // end of PhoneNumber_Test