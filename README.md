# Scilearn challenge, Artem Morozov, February 2020
Interview tasks for Scientific Learning, can be found here:
https://bitbucket.org/snippets/scilearn/xez77B

Challenge completed in IntelliJ IDEA environment, all IDE specific files will be ignored during push.

Used Maven set up for project, pom.xml has everything needed to run tests with JUnit but IDE may request to download jars.

Author: <a href="https://www.linkedin.com/in/morozovartem/">Artem Morozov</a>