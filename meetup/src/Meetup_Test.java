import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by @author Artem Morozov on 3/3/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Meetup_Test {
    private Meetup meetup = new Meetup();
    private LocalDate expected;

    @Test
    public void test_Negative11111_year() {
        expected = LocalDate.of(-11111, 1, 3);
        assertEquals(expected, meetup.meetupDay(-11111, 1, "MONDAY", "First"));
    }

    @Test
    public void test_Three_Digits_Year() {
        expected = LocalDate.of(111, 1, 5);
        assertEquals(expected, meetup.meetupDay(111, 1, "MONDAY", "First"));
    }

    //beginning of FIRST
    @Test
    public void test_First_Monday_Of_January_2020() {
        expected = LocalDate.of(2020, 1, 6);
        assertEquals(expected, meetup.meetupDay(2020, 1, "MONDAY", "First"));
    }

    @Test
    public void test_First_Tuesday_Of_February_2020() {
        expected = LocalDate.of(2020, 2, 4);
        assertEquals(expected, meetup.meetupDay(2020, 2, "tuesday", "First"));
    }

    @Test
    public void test_First_Wednesday_Of_March_2020() {
        expected = LocalDate.of(2020, 3, 4);
        assertEquals(expected, meetup.meetupDay(2020, 3, "Wednesday", "first"));
    }

    @Test
    public void test_First_Thursday_Of_April_2020() {
        expected = LocalDate.of(2020, 4, 2);
        assertEquals(expected, meetup.meetupDay(2020, 4, "thursDAY", "FIRST"));
    }

    @Test
    public void test_First_Friday_Of_May_2020() {
        expected = LocalDate.of(2020, 5, 1);
        assertEquals(expected, meetup.meetupDay(2020, 5, "FRIday", "FIrST"));
    }

    @Test
    public void test_First_Saturday_Of_June_2020() {
        expected = LocalDate.of(2020, 6, 6);
        assertEquals(expected, meetup.meetupDay(2020, 6, "saturday", "first"));
    }

    @Test
    public void test_First_Sunday_Of_July_2020() {
        expected = LocalDate.of(2020, 7, 5);
        assertEquals(expected, meetup.meetupDay(2020, 7, "sunday", "first"));
    } //End of FIRST

    //beginning of SECOND
    @Test
    public void test_Second_Monday_Of_August_2020() {
        expected = LocalDate.of(2020, 8, 10);
        assertEquals(expected, meetup.meetupDay(2020, 8, "monday", "second"));
    }

    @Test
    public void test_Second_Tuesday_Of_September_2020() {
        expected = LocalDate.of(2020, 9, 8);
        assertEquals(expected, meetup.meetupDay(2020, 9, "tuesday", "second"));
    }

    @Test
    public void test_Second_Wednesday_Of_October_2020() {
        expected = LocalDate.of(2020, 10, 14);
        assertEquals(expected, meetup.meetupDay(2020, 10, "wednesday", "second"));
    }

    @Test
    public void test_Second_Thursday_Of_November_2020() {
        expected = LocalDate.of(2020, 11, 12);
        assertEquals(expected, meetup.meetupDay(2020, 11, "thursday", "second"));
    }

    @Test
    public void test_Second_Friday_Of_December_2020() {
        expected = LocalDate.of(2020, 12, 11);
        assertEquals(expected, meetup.meetupDay(2020, 12, "friday", "second"));
    }

    @Test
    public void test_Second_Saturday_Of_January_2020() {
        expected = LocalDate.of(2020, 1, 11);
        assertEquals(expected, meetup.meetupDay(2020, 1, "saturday", "second"));
    }

    @Test
    public void test_Second_Sunday_Of_February_2020() {
        expected = LocalDate.of(2020, 2, 9);
        assertEquals(expected, meetup.meetupDay(2020, 2, "sunday", "second"));
    } //end of SECOND

    //beginning of THIRD
    @Test
    public void test_Third_Monday_Of_March_2020() {
        expected = LocalDate.of(2020, 3, 16);
        assertEquals(expected, meetup.meetupDay(2020, 3, "monday", "third"));
    }

    @Test
    public void test_Third_Tuesday_Of_April_2020() {
        expected = LocalDate.of(2020, 4, 21);
        assertEquals(expected, meetup.meetupDay(2020, 4, "tuesday", "third"));
    }

    @Test
    public void test_Third_Wednesday_Of_May_2020() {
        expected = LocalDate.of(2020, 5, 20);
        assertEquals(expected, meetup.meetupDay(2020, 5, "wednesday", "third"));
    }

    @Test
    public void test_Third_Thursday_Of_June_2020() {
        expected = LocalDate.of(2020, 6, 18);
        assertEquals(expected, meetup.meetupDay(2020, 6, "thursday", "third"));
    }

    @Test
    public void test_Third_Friday_Of_July_2020() {
        expected = LocalDate.of(2020, 7, 17);
        assertEquals(expected, meetup.meetupDay(2020, 7, "friday", "third"));
    }

    @Test
    public void test_Third_Saturday_Of_August_2020() {
        expected = LocalDate.of(2020, 8, 15);
        assertEquals(expected, meetup.meetupDay(2020, 8, "saturday", "third"));
    }

    @Test
    public void test_Third_Sunday_Of_September_2020() {
        expected = LocalDate.of(2020, 9, 20);
        assertEquals(expected, meetup.meetupDay(2020, 9, "sunday", "third"));
    } //end of THIRD

    //beginning of FOURTH
    @Test
    public void test_Fourth_Monday_Of_October_2020() {
        expected = LocalDate.of(2020, 10, 26);
        assertEquals(expected, meetup.meetupDay(2020, 10, "monday", "fourth"));
    }

    @Test
    public void test_Fourth_Tuesday_Of_November_2020() {
        expected = LocalDate.of(2020, 11, 24);
        assertEquals(expected, meetup.meetupDay(2020, 11, "tuesday", "fourth"));
    }

    @Test
    public void test_Fourth_Wednesday_Of_December_2020() {
        expected = LocalDate.of(2020, 12, 23);
        assertEquals(expected, meetup.meetupDay(2020, 12, "wednesday", "fourth"));
    }

    @Test
    public void test_Fourth_Thursday_Of_January_2020() {
        expected = LocalDate.of(2020, 1, 23);
        assertEquals(expected, meetup.meetupDay(2020, 1, "thursday", "fourth"));
    }

    @Test
    public void test_Fourth_Friday_Of_February_2020() {
        expected = LocalDate.of(2020, 2, 28);
        assertEquals(expected, meetup.meetupDay(2020, 2, "friday", "fourth"));
    }

    @Test
    public void test_Fourth_Saturday_Of_March_2020() {
        expected = LocalDate.of(2020, 3, 28);
        assertEquals(expected, meetup.meetupDay(2020, 3, "saturday", "fourth"));
    }

    @Test
    public void test_Fourth_Sunday_Of_April_2020() {
        expected = LocalDate.of(2020, 4, 26);
        assertEquals(expected, meetup.meetupDay(2020, 4, "sunday", "fourth"));
    } //end of FOURTH

    //beginning of LAST
    @Test
    public void test_Last_Monday_Of_May_2020() {
        expected = LocalDate.of(2020, 5, 25);
        assertEquals(expected, meetup.meetupDay(2020, 5, "monday", "last"));
    }

    @Test
    public void test_Last_Tuesday_Of_June_2020() {
        expected = LocalDate.of(2020, 6, 30);
        assertEquals(expected, meetup.meetupDay(2020, 6, "tuesday", "last"));
    }

    @Test
    public void test_Last_Wednesday_Of_July_2020() {
        expected = LocalDate.of(2020, 7, 29);
        assertEquals(expected, meetup.meetupDay(2020, 7, "wednesday", "last"));
    }

    @Test
    public void test_Last_Thursday_Of_August_2020() {
        expected = LocalDate.of(2020, 8, 27);
        assertEquals(expected, meetup.meetupDay(2020, 8, "thursday", "last"));
    }

    @Test
    public void test_Last_Friday_Of_September_2020() {
        expected = LocalDate.of(2020, 9, 25);
        assertEquals(expected, meetup.meetupDay(2020, 9, "friday", "last"));
    }

    @Test
    public void test_Last_Saturday_Of_October_2020() {
        expected = LocalDate.of(2020, 10, 31);
        assertEquals(expected, meetup.meetupDay(2020, 10, "saturday", "last"));
    }

    @Test
    public void test_Last_Sunday_Of_November_2020() {
        expected = LocalDate.of(2020, 11, 29);
        assertEquals(expected, meetup.meetupDay(2020, 11, "sunday", "last"));
    } //end of LAST

    //beginning of TEENTH
    @Test
    public void test_Teenth_Monday_Of_May_2020() {
        expected = LocalDate.of(2020, 5, 18);
        assertEquals(expected, meetup.meetupDay(2020, 5, "monday", "teenth"));
    }

    @Test
    public void test_Teenth_Tuesday_Of_June_2020() {
        expected = LocalDate.of(2020, 6, 16);
        assertEquals(expected, meetup.meetupDay(2020, 6, "tuesday", "teenth"));
    }

    @Test
    public void test_Teenth_Wednesday_Of_July_2020() {
        expected = LocalDate.of(2020, 7, 15);
        assertEquals(expected, meetup.meetupDay(2020, 7, "wednesday", "teenth"));
    }

    @Test
    public void test_Teenth_Thursday_Of_August_2020() {
        expected = LocalDate.of(2020, 8, 13);
        assertEquals(expected, meetup.meetupDay(2020, 8, "thursday", "teenth"));
    }

    @Test
    public void test_Teenth_Friday_Of_September_2020() {
        expected = LocalDate.of(2020, 9, 18);
        assertEquals(expected, meetup.meetupDay(2020, 9, "friday", "teenth"));
    }

    @Test
    public void test_Teenth_Saturday_Of_October_2020() {
        expected = LocalDate.of(2020, 10, 17);
        assertEquals(expected, meetup.meetupDay(2020, 10, "saturday", "teenth"));
    }

    @Test
    public void test_Teenth_Sunday_Of_November_2020() {
        expected = LocalDate.of(2020, 11, 15);
        assertEquals(expected, meetup.meetupDay(2020, 11, "sunday", "teenth"));
    } //end of TEENTH
} // end of Meetup_Test