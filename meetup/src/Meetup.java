import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

/**
 * Created by @author Artem Morozov on 3/1/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Meetup {
    public Meetup() {
    }

    /**@param year int specified by used within LocalDate limits
     * @param month int specified by user to pick a month from 1 to 12
     * @param day specified by user day of the week, used as DayOfWeek enum
     * @param schedule enums from scheduleEnums
     * @return date in yyyy-mm-dd format, if does not pass check in if statement for input then return null
     */
    protected LocalDate meetupDay(int year, int month, String day, String schedule) {
        if ((year < -999999999 || year > 999999999) || (month < 1 || month > 12)
                || day.trim().isEmpty() || schedule.trim().isEmpty()) {
            System.out.println("invalid input, try again");
            return null;
        }
        LocalDate date = LocalDate.of(year, month, 1);
        // adds ability to type in lower case as well
        day = day.toUpperCase();
        schedule = schedule.toUpperCase();

        switch (scheduleEnums.valueOf(schedule)) {
            case FIRST:
                return date.with(TemporalAdjusters.firstInMonth(DayOfWeek.valueOf(day)));
            case SECOND:
                return date.with(TemporalAdjusters.dayOfWeekInMonth(2, DayOfWeek.valueOf(day)));
            case THIRD:
                return date.with(TemporalAdjusters.dayOfWeekInMonth(3, DayOfWeek.valueOf(day)));
            case FOURTH:
                return date.with(TemporalAdjusters.dayOfWeekInMonth(4, DayOfWeek.valueOf(day)));
            case LAST:
                return date.with(TemporalAdjusters.lastInMonth(DayOfWeek.valueOf(day)));
            default:
                date = date.with(TemporalAdjusters.firstInMonth(DayOfWeek.valueOf(day)));
                while (!(date.getDayOfMonth() >= 13 && date.getDayOfMonth() <= 19)) {
                    date = date.with(TemporalAdjusters.next(DayOfWeek.valueOf(day)));
                }
                return date;
        }
    }

    private enum scheduleEnums {
        FIRST,
        SECOND,
        THIRD,
        FOURTH,
        LAST,
        TEENTH
    }
} // end of meetup